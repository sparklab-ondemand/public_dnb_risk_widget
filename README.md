# DNB Risk Widget
This widget serves as an example application to display D&B Company Information and Risk Scores.

## Direct 2.0 Services
The following Direct 2.0 services are used for this application:

- [Entity List Service (FindCompany)](http://developer.dnb.com/docs/2.0/entitylist/6.0)

- [Detailed Company Profile Standard (DCP_STD)](http://developer.dnb.com/docs/2.0/firmographic/3.0)

- [Viability Rating (VIAB_RAT)](http://developer.dnb.com/docs/2.0/assessment/3.0)

- [Financial Stress Score (PBR_FSS_V7.1)](http://developer.dnb.com/docs/2.0/assessment/3.0)

- [Commercial Credit Score (PPR_CCS_V9)](http://developer.dnb.com/docs/2.0/assessment/3.0)

- [Paydex (PIAP_STD)](http://developer.dnb.com/docs/2.0/tradedetail/3.0)

- [Supplier Evaluation Risk (SER)](http://developer.dnb.com/docs/2.0/assessment/3.0)

## Architecture & Libraries
The high-level architecture of this application is fairly simple.

- The web server application
	-In this case, we're using Django as the main web application stack/framework.

- The web server itself.  
	- In production, it is recommended to run Django behind Apache or Nginx.

- The Direct 2.0 Proxy (server-side)
	- Javascript cross-domain policies prevent the client application from communicating directly with Direct 2.0 web services.
	- This is a "dumb proxy" which relays all calls to Direct 2.0 and returns data to the client.
	- This proxy is secured, with many options existing within the proxy settings.
	- See the README.md specific to the Direct2_Proxy.

- The client application
	- This is the web app itself, HTML, CSS, Javascript.
	- The following client libraries are used for this project: 
		- JQuery
		- Twitter Bootstrap
		- Twitter Typeahead (the autocomplete company field)
		- Handlebars (used for templates in the autcomplete's suggestions)
		- KnockoutJS (used for javascript data bindings)
		- Swipebox (used for image display - google images)

## Screenshots

![Screen Shot 2014-06-24 at 5.25.53 PM.png](https://bitbucket.org/repo/nGo74y/images/1262725137-Screen%20Shot%202014-06-24%20at%205.25.53%20PM.png)
![Screen Shot 2014-06-24 at 5.26.11 PM.png](https://bitbucket.org/repo/nGo74y/images/3077388092-Screen%20Shot%202014-06-24%20at%205.26.11%20PM.png)