var MODEL_VIEW;
var SEARCH_RESULT_CACHE;
var SCORE_SELECTED;

/* Google Map Vars */
var map;
var geocoder;
var marker;
var placeService;
/* End Map Vars */

// GOOGLE MAPS
function mapLoaded(){ mapApiLoaded = true; }

function loadGoogleMaps() {
    var script = document.createElement('script');
    script.type = 'text/javascript';
    script.src = 'https://maps.googleapis.com/maps/api/js?libraries=places&key=REMOVED&sensor=false&callback=mapLoaded'; // REMOVED API KEY
    document.body.appendChild(script);
}

// Aryncrhonous map loading requires a callback

function initializeMap(companyName, startAddress) {
    if(!map){
        var mapOptions = {
            zoom: 13,
            center: new google.maps.LatLng(-34.397, 150.644)
        };

        map = new google.maps.Map(document.getElementById('map-canvas'),
          mapOptions);

        geocoder = new google.maps.Geocoder();
        placeService = new google.maps.places.PlacesService(map);
    }

    // Get rid of existing thumbnails.
    $('#imageDiv').empty();
    $('#imageDivTagline').remove();

    if(startAddress)
        codeAddress(companyName, startAddress);

}

// will search company, adjust map, and drop marker.
function codeAddress(companyName, address) {
    geocoder.geocode( {address: address}, function(results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
            map.setCenter(results[0].geometry.location);
            if(!marker)
                marker = new google.maps.Marker({ map: map, position: results[0].geometry.location });
            else
                marker.setPosition(results[0].geometry.location);
        }
    });

    if(companyName){
        placeService.textSearch({query:companyName + ' ' + address}, function(results, status){
            if(status == google.maps.places.PlacesServiceStatus.OK){
                placeService.getDetails({'reference': results[0].reference}, function(results, status){
                    var $imgDiv = $('#imageDiv');
                    var $mapContainer = $('#mapContainer');
                    var count = 0;
                    // appends the top 3 photos to the image div.
                    if(results.photos){
                        //$imgDiv.append('<hr class="visible-xs">');
                        $.each(results.photos, function(index, value){
                            var imgTag = '<div class="col-sm-4 col-md-3 dnb-widget"><div class="dnb-widget-inner"><div class="dnb-widget-content"><a href="' + value.getUrl({maxWidth: 1024, maxHeight: 1024}) + '" class="swipebox" title="'+ companyName +'">';
                            imgTag += '<span style="display:block;width:100%;height:200px;background:url(\'' + value.getUrl({maxWidth: 450}) + '\') 50% 50% no-repeat"></span></a></div></div></div>';
                            $imgDiv.append(imgTag);
                            count++;
                            if(count > 3) // only grab first 4 images
                                return false;
                        });

                        $imgDiv.parent().append('<div class="dnb-widget-row" id="imageDivTagline"><div class="dnb-images-tagline"><span class="text-muted">Images provided by Google Maps</span></div></div>');
                    }

                    var center = map.getCenter(); // store map center, for case of resizing.
                    if(count == 0){ // if no images, hide image div and expand map container
                        $imgDiv.hide();
                    }
                    else{ // else shrink the map container and show the image div
                        $imgDiv.show();
                        $(".swipebox").swipebox();
                    }
                    // resize and re-center the map
//                    google.maps.event.trigger(map, 'resize');
                    map.setCenter(center);
                });
            }
        });
    }
}
// END GOOGLE MAPS

// The main ViewModel for KnockoutJS.  A collection of 7 independent models.
function CreditWidgetViewModel() {
    this.companyInfo = new KnockoutWrapper();
    this.companyDetails = new KnockoutWrapper();
    this.viabilityInfo = new KnockoutWrapper();
    this.fssInfo = new KnockoutWrapper();
    this.ccsInfo = new KnockoutWrapper();
    this.paydexInfo = new KnockoutWrapper();
    this.serInfo = new KnockoutWrapper();
};

// Shows the details on any given score.  Invoked when a score is selected.
function showDetails(id, scoreBar){
    $('.dnb-details').each(function(index){
        var id2 = id + 'Mobile';
        if(this.id != id && this.id != id2){
            $(this).hide();
        }
        else {
            $(this).show();
        }
    });
    $(".score-clicker").parents("tr").removeClass("active-score");
    $(scoreBar).parents("tr").addClass("active-score");

    var $ele = $('#' + id);
    if ($ele.length > 0) // Scroll to element if we found one.
        scrollTo($ele);
}

// Populates the company details from the Direct 2.0 response.
function populateCompanyDetails(result){
    result = $.parseJSON(result);

    var usSic = 'unknown';
    var usSicText = 'unknown';
    var naics = 'unknown';
    var naicsText = 'unknown';

    // find industries
    var industries = getJsonElement(result, 'OrderProductResponse.OrderProductResponseDetail.Product.Organization.IndustryCode.IndustryCode');
    $.each(industries, function(index, value){ // loop and find first US SIC and first NAICS codes
        try{
            if(usSic == 'unknown' && value['@DNBCodeValue'] == 399){
                usSic = value.IndustryCode.$;
                usSicText = value.IndustryCodeDescription[0].$;
            }
            else if(naics == 'unknown' && value['@DNBCodeValue'] == 700){
                naics = value.IndustryCode.$;
                naicsText = value.IndustryCodeDescription[0].$;
            }

            if(usSic != 'unknown' && naics != 'unknown') //break loop if both found
                return false;
        } catch(err) {
            return false;
        }
    });

    // add http:// if needed, to force link external
    var website = getJsonElement(result, 'OrderProductResponse.OrderProductResponseDetail.Product.Organization.Telecommunication.WebPageAddress[0].TelecommunicationAddress');
    if(website.length > 0 && website.substring(0, 7).toLowerCase() !== 'http://')
        website = 'http://' + website;

    var info = {
        SummaryText: getJsonElement(result, 'OrderProductResponse.OrderProductResponseDetail.Product.Organization.SubjectHeader.OrganizationSummaryText'),
        Revenue: getJsonElement(result, 'OrderProductResponse.OrderProductResponseDetail.Product.Organization.Financial.KeyFinancialFiguresOverview[0].SalesRevenueAmount.$'),
        RevenueUnits: getJsonElement(result, 'OrderProductResponse.OrderProductResponseDetail.Product.Organization.Financial.KeyFinancialFiguresOverview[0].SalesRevenueAmount.@UnitOfSize'),
        NumEmployees: getJsonElement(result, 'OrderProductResponse.OrderProductResponseDetail.Product.Organization.EmployeeFigures.IndividualEntityEmployeeDetails.TotalEmployeeQuantity', 'unknown'),
        EmployeeGrowth: getJsonElement(result, 'OrderProductResponse.OrderProductResponseDetail.Product.Organization.Financial.KeyFinancialFiguresOverview[0].EmployeeQuantityGrowthRate', 'unknown'),
        YearFounded: getJsonElement(result, 'OrderProductResponse.OrderProductResponseDetail.Product.Organization.OrganizationDetail.OrganizationStartYear', 'unknown'),
        USSIC: usSic,
        USSICText: usSicText,
        NAICS: naics,
        NAICSText: naicsText,
        Website: website
    };

    MODEL_VIEW.companyDetails.data(info);
    MODEL_VIEW.companyDetails.setStatus('done');
}

// Populates the viability details from the Direct 2.0 response.
function populateViability(result){
    try{
        result = $.parseJSON(result);
        var viab = getJsonElement(result, 'OrderProductResponse.OrderProductResponseDetail.Product.Organization.Assessment.DNBViabilityRating[0].DNBViabilityRating');

        var viabResult = flatten(result.OrderProductResponse.OrderProductResponseDetail.Product.Organization.Assessment.DNBViabilityRating.ViabilityScore);
        var portResult = flatten(result.OrderProductResponse.OrderProductResponseDetail.Product.Organization.Assessment.DNBViabilityRating.PortfolioComparisonScore);
        var depthResult = flatten(result.OrderProductResponse.OrderProductResponseDetail.Product.Organization.Assessment.DNBViabilityRating.DataDepthDetail);
        var profileResult = flatten(result.OrderProductResponse.OrderProductResponseDetail.Product.Organization.Assessment.DNBViabilityRating.OrganizationProfileDetail);

        var info = {
            ViabilityScore: {
                ClassScore: viab.substring(0, 1),
                Detail: viabResult
            },
            PortfolioScore: {
                ClassScore: viab.substring(1, 2),
                Detail: portResult
            },
            DataDepthScore: {
                Indicator: viab.substring(2, 3),
                Detail: depthResult
            },
            CompanyProfileScore: {
                ProfileRating: viab.substring(3, 4),
                Detail: profileResult
            }
        }

        MODEL_VIEW.viabilityInfo.data(info);
        MODEL_VIEW.viabilityInfo.setStatus('done');
        if(!SCORE_SELECTED && $(window).width() > 768)
            $('#viabilityWell').click();
    }
    catch(err){
        MODEL_VIEW.viabilityInfo.setStatus('error');
    }
}

// Populates the fss details from the Direct 2.0 response.
function populateFSS(result){
    try {
        result = $.parseJSON(result);
        var score = getJsonElement(result, 'OrderProductResponse.OrderProductResponseDetail.Product.Organization.Assessment.FinancialStressScore[0].ClassScore');
        var percentile = getJsonElement(result, 'OrderProductResponse.OrderProductResponseDetail.Product.Organization.Assessment.FinancialStressScore[0].NationalPercentile');
        var detail = flatten(result.OrderProductResponse.OrderProductResponseDetail.Product.Organization.Assessment.FinancialStressScore[0]);

        if(score){
            MODEL_VIEW.fssInfo.data({Score:score, Percentile: percentile, Detail: detail});
            MODEL_VIEW.fssInfo.setStatus('done');
            if(!SCORE_SELECTED && $(window).width() > 768)
                $('#fssWell').click();
        } else {
            MODEL_VIEW.fssInfo.setStatus('error');
        }
    } catch(err){
        MODEL_VIEW.fssInfo.setStatus('error');
    }
}

// Populates the ccs details from the Direct 2.0 response.
function populateCCS(result){
    try {
        result = $.parseJSON(result);
        var score = getJsonElement(result, 'OrderProductResponse.OrderProductResponseDetail.Product.Organization.Assessment.CommercialCreditScore[0].ClassScore');
        var percentil = getJsonElement(result, 'OrderProductResponse.OrderProductResponseDetail.Product.Organization.Assessment.CommercialCreditScore[0].NationalPercentile');
        var detail = flatten(result.OrderProductResponse.OrderProductResponseDetail.Product.Organization.Assessment.CommercialCreditScore[0]);

        if(score){
            MODEL_VIEW.ccsInfo.data({Score:score, Percentile: percentil, Detail: detail});
            MODEL_VIEW.ccsInfo.setStatus('done');
            if(!SCORE_SELECTED && $(window).width() > 768)
                $('#ccsWell').click();
        } else {
            MODEL_VIEW.ccsInfo.setStatus('error');
        }
    } catch(err) {
        MODEL_VIEW.cssInfo.setStatus('error');
    }

}

// Populates the paydex details from the Direct 2.0 response.
function populatePaydex(result){
    try {
        result = $.parseJSON(result);
        var score = getJsonElement(result, 'OrderProductResponse.OrderProductResponseDetail.Product.Organization.BusinessTrading.Purchaser.CurrentPaydexScore.TwelveMonthsPaydex.PaydexScore');
        var detailBlock = flatten(result.OrderProductResponse.OrderProductResponseDetail.Product.Organization.BusinessTrading.Purchaser);

        if(score){
            MODEL_VIEW.paydexInfo.data({Score:score, Detail: detailBlock});
            MODEL_VIEW.paydexInfo.setStatus('done');
            if(!SCORE_SELECTED && $(window).width() > 768)
                $('#paydexWell').click();
        }
        else {
            MODEL_VIEW.paydexInfo.setStatus('error');
        }
    } catch(err) {
        MODEL_VIEW.paydexInfo.setStatus('error');
    }
}

// Populates the ser details from the Direct 2.0 response.
function populateSer(result){
    try {
        result = $.parseJSON(result);
        var score = getJsonElement(result, 'OrderProductResponse.OrderProductResponseDetail.Product.Organization.Assessment.SupplierEvaluationRiskScore[0].RiskScore');
        var detailBlock = flatten(result.OrderProductResponse.OrderProductResponseDetail.Product.Organization.Assessment.SupplierEvaluationRiskScore);

        if(score){
            MODEL_VIEW.serInfo.data({Score:score, Detail: detailBlock});
            MODEL_VIEW.serInfo.setStatus('done');
            if(!SCORE_SELECTED && $(window).width() > 768)
                $('#serWell').click();
        }
        else {
            MODEL_VIEW.serInfo.setStatus('error');
        }
    } catch(err) {
        MODEL_VIEW.serInfo.setStatus('error');
    }
}

// Normalizes the viability score and assigns a color value for the hexagon.
function viabScoreToColor(data){
    var normalizedScore = normalizeScore(data, 1, 9, true);
    return colorValToColor(normalizedScore);
}

// Normalizes the viability score and assigns a percentage value for the slider.
function viabScoreToPct(data){
    var normalizedScore = normalizeScore(data, 1, 9, true);
    return (normalizedScore*100) + '%';
}
// Normalizes the fss score and assigns a color value for the hexagon.
function fssScoreToColor(data){
    var normalizedScore = normalizeScore(data, 0, 5, true);
    return colorValToColor(normalizedScore);
}

// Normalizes the fss score and assigns a color value for the hexagon.
function fssScoreToPct(data){
    var normalizedScore = normalizeScore(data, 0, 5, true);
    return (normalizedScore*100) + '%';
}

// Normalizes the ccs score and assigns a color value for the hexagon.
function ccsScoreToColor(data){
    return fssScoreToColor(data);
}

// Normalizes the ccs score and assigns a color value for the hexagon.
function ccsScoreToPct(data){
    return fssScoreToPct(data);
}

// Normalizes the ser score and assigns a color value for the hexagon.
function serScoreToColor(data){
    var normalizedScore = normalizeScore(data, 1, 9, true);
    return colorValToColor(normalizedScore);
}

// Normalizes the ser score and assigns a color value for the hexagon.
function serScoreToPct(data){
    var normalizedScore = normalizeScore(data, 1, 9, true);
    return (normalizedScore*100) + '%';
}

// Normalizes the paydex score and assigns a color value for the hexagon.
function paydexScoreToColor(data){
    var normalizedScore = normalizeScore(data, 0, 100, false);
    return colorValToColor(normalizedScore);
}

// Normalizes the paydex score and assigns a color value for the hexagon.
function paydexScoreToPct(data){
    var normalizedScore = normalizeScore(data, 0, 100, false);
    return (normalizedScore*100) + '%';
}

// Converts the unformatted revenue to a formatted value.  Rounds to 2 decimal points, converts to billions.
function formatRevenue(revenue, units){
    if(revenue.length == 0)
        return 'unknown';

    var unitText = '';

    if(units === 'SingleUnits'){
        if(revenue > 100000){
            revenue = revenue/1000000;
            units = "Million"
        }
    }

    if(units === "Million"){
        if(revenue > 1000){
            revenue = revenue/1000;
            unitText = "Bil";
        } else {
            unitText = "Mil";
        }
    }

    revenue = Math.round(revenue*100)/100; // limit to 2 decimal places
    return '$' + revenue + ' ' + unitText;
}

// Shows the detail modal when 'How was this determined?" buttons are clicked.
function showDetailedModal(dataObj, $header, $pBlurb){
    $('#modalTitle').text($header.text());
    var $modalBody = $('#modalBody');
    $modalBody.empty();
    $modalBody.append($pBlurb);
    $modalBody.append('<p>The statement above is a summary of the details below:</p>');
    var $detailsBlock = '<div class="row"><div class="dnb-details-table col-sm-10 col-sm-offset-1">';
    $detailsBlock += '<table class="table"><tbody>';
    $.each(dataObj, function(index, ele){
        if(ele.value != null){
            $detailsBlock += '<tr>';
            $detailsBlock += '<td style="padding-left: ' + 20*ele.indent + 'px;">' + ele.friendlyName + '</td>';
            $detailsBlock += '<td>' + ele.value + '</td>';
            $detailsBlock += '</tr>';
        }
    });
    $detailsBlock += '</tbody></table></div></div>';
    $modalBody.append($detailsBlock);
    $('#detailsModal').modal();
}

// Scrolls animatedly to the given element.
function scrollTo($ele){
    $('html, body').animate({
       scrollTop: $ele.offset().top
    }, 400);
}

// Used for the Twitter Typeahead, called when autocomplete or selection is made.
function handleSelection(event, selected, dataset){
    MODEL_VIEW.companyInfo.data(selected);
    MODEL_VIEW.viabilityInfo.setStatus('initial');
    SCORE_SELECTED = false;

    MODEL_VIEW.companyDetails.setStatus('loading');
    // details
    $.ajax( direct2_proxy + "V3.0/organizations/" + selected.DunsNumber + "/products/DCP_STD")
        .done(populateCompanyDetails)
        .fail(function() { MODEL_VIEW.companyDetails.setStatus('error'); });

    MODEL_VIEW.companyInfo.setStatus('done');

    initializeMap(selected.OrganizationName, selected.Street + ' ' + selected.City + ', ' +
            selected.Territory + ' ' + selected.PostalCode);

    $('#companyField').blur();
    $('#infoTab').click();
}

// Run Code
$(function() {
    SEARCH_RESULT_CACHE = {}
    MODEL_VIEW = new CreditWidgetViewModel();

    // Twitter Typeahead
    var $companyField = $('#companyField');

    // Bloodhound is used for the Twitter Typeahead suggestion engine
    var companies = new Bloodhound({
        datumTokenizer: function(datum){},
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        remote: {
            url: direct2_proxy + "V4.0/organizations?findcompany=true&KeywordText=%QUERY",
            ajax: {
                beforeSend: function(){ $('#companyField').addClass('loading'); }, // add loading class for spinner
                complete: function(){ $('#companyField').removeClass('loading'); } // remove loading class
            },
            // filters the Direct 2.0 raw results into simple JSON
            filter: function(parsedResponse){
                var results = getJsonElement(parsedResponse, 'FindCompanyResponse.FindCompanyResponseDetail.FindCandidate');
                var toReturn = [];
                $.each(results, function(index, object){
                    var result = {
                        'DunsNumber': getJsonElement(object, 'DUNSNumber'),
                        'OrganizationName': getJsonElement(object, 'OrganizationPrimaryName.OrganizationName.$'),
                        'LocationType': getJsonElement(object, 'FamilyTreeMemberRole[0].FamilyTreeMemberRoleText.$', 'Unknown Type'),
                        'City': getJsonElement(object, 'PrimaryAddress.PrimaryTownName', 'Unknown'),
                        'Territory': getJsonElement(object, 'PrimaryAddress.TerritoryOfficialName', 'Unknown'),
                        'Street': getJsonElement(object, 'PrimaryAddress.StreetAddressLine[0].LineText'),
                        'PostalCode': getJsonElement(object, 'PrimaryAddress.PostalCode'),
                        'Country': getJsonElement(object, 'PrimaryAddress.CountryISOAlpha2Code')
                    }
                    toReturn.push(result);
                });
                return toReturn;
            }
        },
        limit: 10
    });

    companies.initialize();

    // Template language below based off Handlebars.js, not Django.  Django templating will not parse this static file.
    var suggestionDiv = [
            '<div class="typeahead-suggestion">',
            '<span><strong>{{ OrganizationName }}</strong></span> - <span class="text-muted">{{ LocationType }}</span><br/>',
            '<span class="sub-text">{{ City }}, {{ Territory }}</span><br/>',
            '</div>'
    ].join('\n');

    // The actual typeahead declaration
    $companyField.typeahead(null, {
        name: 'company-search',
        displayKey: 'OrganizationName',
        source: companies.ttAdapter(),
        templates: {
            empty: [
                '<div class="tt-no-suggestions">',
                'No companies matching your query',
                '</div>'
            ].join('\n'),
            suggestion: Handlebars.compile(suggestionDiv)
        }
    }).on('typeahead:selected', handleSelection)
        .on('typeahead:autocompleted', handleSelection);

    $companyField.focus();
    $companyField.on("click", function () {
       $(this).select();
    });


    // End Twitter Typeahead

    // Load all scores asynchronously when the assessment tab is selected
    $(document).on("click", "#assessmentTab", function(){
        // do nothing if data has already been loaded.
        if(MODEL_VIEW.viabilityInfo.status() != 'initial')
            return;
        showDetails('none');
        MODEL_VIEW.viabilityInfo.setStatus('loading');
        MODEL_VIEW.fssInfo.setStatus('loading');
        MODEL_VIEW.ccsInfo.setStatus('loading');
        MODEL_VIEW.paydexInfo.setStatus('loading');
        MODEL_VIEW.serInfo.setStatus('loading');

        var duns = MODEL_VIEW.companyInfo.data().DunsNumber;

        // viability
        $.ajax( direct2_proxy + "V3.0/organizations/" + duns + "/products/VIAB_RAT")
            .done(populateViability)
            .fail(function() { MODEL_VIEW.viabilityInfo.setStatus('error'); });

        // fss
        $.ajax( direct2_proxy + "V3.0/organizations/"+ duns + "/products/PBR_FSS_V7.1")
            .done(populateFSS)
            .fail(function() { MODEL_VIEW.fssInfo.setStatus('error'); });

        // ccs
        $.ajax( direct2_proxy + "V3.0/organizations/"+ duns + "/products/PPR_CCS_V9")
            .done(populateCCS)
            .fail(function() { MODEL_VIEW.ccsInfo.setStatus('error'); });

        // paydex
        $.ajax( direct2_proxy + "V3.0/organizations/" + duns + "/products/PIAP_STD")
            .done(populatePaydex)
            .fail(function() { MODEL_VIEW.paydexInfo.setStatus('error'); });

        // ser
        $.ajax( direct2_proxy + "V3.0/organizations/" + duns + "/products/SER")
            .done(populateSer)
            .fail(function() { MODEL_VIEW.serInfo.setStatus('error'); });
    });

    // show details when clicked.
    $(document).on('click', '#viabilityWell', function(){
        showDetails('viabilityDetails','#viabilityWell');
        SCORE_SELECTED = true;
        return false;
    });

    // show details when clicked.
    $(document).on('click', '#portfolioWell', function(){
        showDetails('portfolioDetails','#portfolioWell');
        SCORE_SELECTED = true;
        return false;
    });

    // show details when clicked.
    $(document).on('click', '#dataDepthWell', function(){
        showDetails('dataDepthDetails','#dataDepthWell');
        SCORE_SELECTED = true;
        return false;
    });

    // show details when clicked.
    $(document).on('click', '#companyProfileWell', function(){
        showDetails('companyProfileDetails','#companyProfileWell');
        SCORE_SELECTED = true;
        return false;
    });

    // show details when clicked.
    $(document).on('click', '#fssWell', function(){
        showDetails('fssDetails','#fssWell');
        SCORE_SELECTED = true;
        return false;
    });

    // show details when clicked.
    $(document).on('click', '#ccsWell', function(){
        showDetails('ccsDetails','#ccsWell');
        SCORE_SELECTED = true;
        return false;
    });

    // show details when clicked.
    $(document).on('click', '#paydexWell', function(){
        showDetails('paydexDetails','#paydexWell');
        SCORE_SELECTED = true;
        return false;
    });

    // show details when clicked.
    $(document).on('click', '#serWell', function(){
        showDetails('serDetails','#serWell');
        SCORE_SELECTED = true;
        return false;
    });

    // Detail buttons
    $(document).on('click', '#viabDetailsBtn', function(ele){
        var $btn = $('#viabDetailsBtn');
        var $header = $btn.parent().parent().children(':first');
        var $blurb = $btn.prevAll('.score-evaluation:visible');
        showDetailedModal(MODEL_VIEW.viabilityInfo.data().ViabilityScore.Detail, $header.clone(), $blurb.clone());
    });
    $(document).on('click', '#portfolioDetailsBtn', function(){
        var $btn = $('#portfolioDetailsBtn');
        var $header = $btn.parent().parent().children(':first');
        var $blurb = $btn.prevAll('.score-evaluation:visible');
        showDetailedModal(MODEL_VIEW.viabilityInfo.data().PortfolioScore.Detail, $header.clone(), $blurb.clone());
    });
    $(document).on('click', '#dataDepthDetailsBtn', function(){
        var $btn = $('#dataDepthDetailsBtn');
        var $header = $btn.parent().parent().children(':first');
        var $blurb = $btn.prevAll('.score-evaluation:visible');
        showDetailedModal(MODEL_VIEW.viabilityInfo.data().DataDepthScore.Detail, $header.clone(), $blurb.clone());
    });
    $(document).on('click', '#fssDetailsBtn', function(){
        var $btn = $('#fssDetailsBtn');
        var $header = $btn.parent().parent().children(':first');
        var $blurb = $btn.prevAll('.score-evaluation:visible');
        showDetailedModal(MODEL_VIEW.fssInfo.data().Detail, $header.clone(), $blurb.clone());
    });
    $(document).on('click', '#ccsDetailsBtn', function(){
        var $btn = $('#ccsDetailsBtn');
        var $header = $btn.parent().parent().children(':first');
        var $blurb = $btn.prevAll('.score-evaluation:visible');
        showDetailedModal(MODEL_VIEW.ccsInfo.data().Detail, $header.clone(), $blurb.clone());
    });
    $(document).on('click', '#paydexDetailsBtn', function(){
        var $btn = $('#paydexDetailsBtn');
        var $header = $btn.parent().parent().children(':first');
        var $blurb = $btn.prevAll('.score-evaluation:visible');
        showDetailedModal(MODEL_VIEW.paydexInfo.data().Detail, $header.clone(), $blurb.clone());
    });
    $(document).on('click', '#serDetailsBtn', function(){
        var $btn = $('#serDetailsBtn');
        var $header = $btn.parent().parent().children(':first');
        var $blurb = $btn.prevAll('.score-evaluation:visible');
        showDetailedModal(MODEL_VIEW.serInfo.data().Detail, $header.clone(), $blurb.clone());
    });

    $(document).on('click', '#infoTab',function(){
       google.maps.event.trigger(map, 'resize');
    });

    // register Knockout Bindings
    ko.applyBindings(MODEL_VIEW);
    loadGoogleMaps();
});