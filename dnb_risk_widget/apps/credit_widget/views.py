# Create your views here.
from django.shortcuts import render
from dnb_risk_widget.apps.common_login.decorators import login_or_direct2_required

@login_or_direct2_required
def app_view(request, logo):
    context = {
        'title':'D&B Risk Assessment',
        'theme':'flatly'
    }

    if logo:
        if logo[0:4].lower() != 'http':
            logo = None
        else:
            try:
                slashIndex = logo[0:8].rindex('/')
                logo = '%s%s' % ('http://', logo[slashIndex+1:])
                context['logo'] = logo
            except:
                pass

    return render(request, 'credit_widget/app.html', context)

