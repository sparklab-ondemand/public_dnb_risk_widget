__author__ = 'Bammel'

AUTHENTICATION_ENDPOINT = 'https://maxcvservices.dnb.com/rest/Authentication'
DIRECT_ENDPOINT = 'https://maxcvservices.dnb.com/'

# Authentication methods include 'session_only', 'stored', and 'stored_open'
# 'session_only' requires session vars 'direct2_x_dnb_user' and 'direct2_x_dnb_pwd' to be set.
# 'stored' will require a django user to be logged in. - if session vars are provided, login isn't required and sessions will be used they will be used instead of stored credentials.
# 'stored_open' is wide open for usage. - if session vars are provided, they will be used instead of stored credentials.
# using session will also utilize (and create) session vars 'direct2_token' and 'direct2_token_time' for token caching.
AUTHENTICATION_METHOD = 'stored'

# Requred for authentication_method = 'stored' and 'stored_open', ignored for 'session_only'.
STORED_DNB_UID = '' # REMOVED - Direct 2.0 credentials
STORED_DNB_PWD = '' # REMOVED - Direct 2.0 credentials

# Time in seconds to cache proxy responses.
# A setting of 0 will disable caching.
PROXY_CACHE_TIME = 15 * 60 # 15 minutes

# Time in seconds to cache a 'stored' authentication token before re-auth
# A setting of 0 will disable caching.
TOKEN_CACHE_TIME = 60 * 60 * 7 # 7 hours

# Maximum time, in seconds, that the proxy will resubmit requests on QPS errors.
# A setting of 0 will disable retries.
QPS_RETRY_TIMEOUT = 10


# Verifies proxy settings
def _verify_proxy_settings():
    try:
        if AUTHENTICATION_METHOD not in ('session_only', 'stored', 'stored_open'):
            raise NameError 
    except NameError:
        raise Exception('Invalid proxy_settings configuration.')

_verify_proxy_settings()
