# Direct 2.0 Django Proxy
This is a clean and simple proxy for Direct 2.0 written in Django as a Django App to facilitate easy re-use.  Features include:

- Proper error handling using appropriate HTTP status codes, and descriptive errors returned in the body as JSON.

- Caching of responses for handling repeated requests quickly.

- Easy configuration via proxy_settings.py, which is validated at launch.

- Support for 'session_only', 'stored', and 'stored_open' authentication modes - all of which cache tokens.

- Support for standard delivery and JSONP delivery, for cross-domain support.

- Completely self-contained, making it easy to drop into any Django project.

- Integration with django_common_login app through passing Django session vars or Django authenticated sessions.

## Installation
This proxy is meant to be dropped into any Django project easily as a Django app:

1) Install the proxy app

The best approach is to clone this repository into your existing GIT Repo (Django Project) as a submodule.
**NOTE:** Use SSH for this, as the remote repo path is checked into your main project, and for deployment, you want SSH.
```
git submodule add git@bitbucket.org:sparklab-ondemand/django_direct2_proxy.git [PATH_TO_APPS_DIRECTLY]/direct2_proxy
```
Then add the app to the INSTALLED_APPS in Django settings.

2) Map your URL as desired
```python
urlpatterns = patterns('',
    ...
    url(r'^proxy/', include('path.to.proxyapp.urls')),
    ...
)
```

3) Enable Django caching (this app will use the default cache)
```python
CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
        'LOCATION': 'maincache'
    }
}
```

## Configuration
All configuration is handled via the proxy_settings.py script.  The settings are well commented and should be self-explanatory.

###Authentication Method
The authentication method is controls the security of the proxy.  It's configured with the *AUTHENTICATION_METHOD* setting, and can be set to any of the following.

####Session_Only
Using this method, the proxy will look for **direct2_x_dnb_user** and **direct2_x_dnb_pwd** in the django request session.  These can be provided via the django_common_login app or another component.
This app will additionally create and/or modify session vars **direct2_token** and **direct2_token_time** for caching.

####Stored
Using this method, the user is expected to be logged into django.  Credentials are stored in the proxy_settings.py file, and will be used for all requests.
If session vars **direct2_x_dnb_user** and **direct2_x_dnb_pwd** are provided, they will be used instead of the stored credentials, and the user need not be logged in.

####Stored_Open
Credentials are stored in the proxy_settings.py file, and will be used for all requests.  The proxy will be wide open for usage.
If session vars **direct2_x_dnb_user** and **direct2_x_dnb_pwd** are provided, they will be used instead of the stored credentials.

###Proxy Cache Time
Time in seconds that the proxy will cache responses.

###Auth Token Cache Time
Time in seconds before an auth token will be used before re-authing against Direct.

###QPS Retry Timeout
Time in seconds that the proxy will retry a failed API call due to QPS errors.

###JSONP
JSONP is supported implicitly by looking for the existence of a 'callback' GET parameter.