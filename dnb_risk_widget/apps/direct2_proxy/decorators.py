__author__ = 'Bammel'

import proxy_settings
from functools import wraps
from django.http.response import HttpResponse
from django.utils.decorators import available_attrs
from django.views.decorators.cache import cache_page

def secure_and_cache_proxy(timeout):
    def decorator(view_func):
        @wraps(view_func, assigned=available_attrs(view_func))
        def _wrapped_view(request, *args, **kwargs):
            session_vars = request.session.keys()

            # Session vars trump all else.
            if ('direct2_x_dnb_user' in session_vars) and ('direct2_x_dnb_pwd' in session_vars):
                if 'direct2_token' in session_vars: # only allow cache if token exists
                    return cache_page(timeout)(view_func)(request, *args, **kwargs)
                else:
                    return view_func(request, *args, **kwargs)
            elif proxy_settings.AUTHENTICATION_METHOD == 'stored':
                if request.user.is_authenticated():
                    return cache_page(timeout)(view_func)(request, *args, **kwargs)
                else:
                    return _throw_error()
            elif proxy_settings.AUTHENTICATION_METHOD == 'stored_open':
                return cache_page(timeout)(view_func)(request, *args, **kwargs)
            else: # setting on 'session' without session vars set
                return _throw_error()

        return _wrapped_view
    return decorator

def _throw_error():
    return HttpResponse('{"error":"Must be logged in to use the proxy."}', status=403)