__author__ = 'Bammel'

from django.http import HttpResponse
import requests
import proxy_settings
from datetime import datetime
from decorators import secure_and_cache_proxy
import json

@secure_and_cache_proxy(proxy_settings.PROXY_CACHE_TIME)
def call_direct(request, path):
    """ The main django view function to make requests to Direct 2.0 """
    auth_token = _get_auth(request)

    # If auth fails, return 401.
    if not auth_token:
        return HttpResponse(status=401)

    query_string = request.META['QUERY_STRING']

    callback = None
    try:
        callback = request.GET['callback']

        #  Remove the callback from the request query string
        query_string = query_string.replace('callback=%s' % (callback), '')
    except KeyError:
        pass


    if len(path) == 0:
        return HttpResponse(_json_error('You must include the query portion of the URL in the GET request'), status=400)

    result = None
    start = datetime.now()
    header = {'authorization':auth_token}

    qps_error = True # Force the loop to run the first time.
    while qps_error:
        qps_error = False

        # Only retry for a maximum of proxy_settings.QPS_RETRY_TIMEOUT_SECONDS seconds:
        # Must run at least once
        if result and (datetime.now() - start).total_seconds() > proxy_settings.QPS_RETRY_TIMEOUT:
            break

        result = requests.get('%s%s?%s' % (proxy_settings.DIRECT_ENDPOINT, path, query_string), headers=header)

        if result.status_code == 401:
            response_code = _get_response_code(result.text)
            if response_code == 'SC006':
                qps_error = True

    # Return JSONP if necessary
    if callback:
        data = '%s(%s);' % (callback, result.text)
        return HttpResponse(data, 'application/javascript', status=result.status_code)

    return HttpResponse(result.text, status=result.status_code)

def _json_error(message):
    """
    Simple helper function to build a standard JSON error string.
    :param message: The error message to be conveyed
    :return: The formatted JSON error
    """
    return '{"error":"%s"}' % (message)

def _get_auth(request):
    """
    Helper function to retrieve the Direct 2.0 authorization token.
    :param request: the django request object
    :return: an auth token or None if auth fails
    """
    uid = pwd =  ''
    token = token_time = None
    using_session = False
    session_vars = request.session.keys()

    if ('direct2_x_dnb_user' in session_vars) and ('direct2_x_dnb_pwd' in session_vars):
        using_session = True
        uid = request.session['direct2_x_dnb_user']
        pwd = request.session['direct2_x_dnb_pwd']

        try:
            token = request.session['direct2_token'] # might fail, okay to fail silently
            token_time = datetime.strptime(request.session['direct2_token_time'],"%Y-%m-%dT%H:%M:%S") # might fail, okay to fail silently
        except Exception as ex:
            pass
    elif proxy_settings.AUTHENTICATION_METHOD == 'stored' or proxy_settings.AUTHENTICATION_METHOD == 'stored_open': # stored or stored_open
        # No need to check django authentication - that's already been taken care of in the @secure_and_cache_proxy decorator.
        try:
            uid = proxy_settings.STORED_DNB_UID
            pwd = proxy_settings.STORED_DNB_PWD
            token = proxy_settings.authcache_token
            token_time = proxy_settings.authcache_timestamp
        except AttributeError:
            pass
    else:
        # Shouldn't be able to hit this condition, it's protected by the @secure_and_cache_proxy decorator.
        return None


    # Check if cached token is expired
    if token and token_time:
        now = datetime.now()
        delta = now - token_time
        # Return the cached token if within the cache time range.
        if delta.total_seconds() < proxy_settings.TOKEN_CACHE_TIME:
            return token

    # Needs to request new authentication token
    token_time = datetime.now()
    headers = {"x-dnb-user":uid, "x-dnb-pwd":pwd}

    result = requests.post(proxy_settings.AUTHENTICATION_ENDPOINT, headers=headers)
    if result.status_code == 200:
        token = result.headers["Authorization"]
        if using_session:
            # Set session vars
            time_string = token_time.isoformat()
            try:
                time_string = time_string[:time_string.index('.')]
            except:
                pass

            request.session['direct2_token'] = token
            request.session['direct2_token_time'] = time_string

        else: # stored or stored_open
            proxy_settings.authcache_token = token
            proxy_settings.authcache_timestamp = token_time

        return token
    # Returned if authentication fails
    return None

def _get_json_field(json, key, default=None):
    """
    Safe helper function to extract json elements, or return None if the requested element does not exist.
    :param json: Decoded JSON stored in a dictionary datatype.  As generated by json.loads(...) for instance
    :param key: The key of the element to find.  Can be separated by pipes, for instance:
    result['OrganizationPrimaryName']['OrganizationName'] -> _get_json_field('OrganizationPrimaryName|OrganizationName')
    :param default: A default to return if the element is not found.  None by default
    :return: The requested element in its native datatype
    """
    if '|' not in key:
        try:
            if '[' in key:
                index = int(key[key.index('[')+1 : key.index(']')])
                key = key[:key.index('[')]

                return json[key][index]
            else:
                return json[key]
        except:
            return default
    else:
        try:
            nextKey = key[key.index('|')+1:]
            key = key[:key.index('|')]
            if '[' in key:
                index = int(key[key.index('[')+1 : key.index(']')])
                key = key[:key.index('[')]

                return _get_json_field(json[key][index], nextKey)
            else:
                return _get_json_field(json[key], nextKey)
        except:
            return default

def _get_response_code(raw_result):
    """
    Helper function which exists to extract the response code from the raw text of a Direct 2.0 response.
    This needs to be done with raw text functions because the parent element may change depending on the call.
    For instance: "FindCompanyResponse":{"TransactionResult":{...}}} vs OrderProductResponse":{"TransactionResult":{...}}}
    """
    result_start = raw_result.index('{', raw_result.index('TransactionResult'))
    result_end = raw_result.index('}', result_start)
    result = raw_result[result_start:result_end+1]

    return _get_json_field(json.loads(result), 'ResultID')