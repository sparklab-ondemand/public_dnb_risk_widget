# Direct 2.0 Django Common Login
This is a clean and simple django app for easily plugging in the standard SparkLab login screens.  It provides everything needed for login pages, utilizing the standard Django auth system or supporting API keys via Django sessions.  The following features are provided:

- Self-contained styled templates for login, and password reset flows (via email).

- Modifies the Django user model to make Email both required and unique.

- Modifies the 'Add User' form in django admin to include the required email field.

- Allows for minor configuration - enabling/disabling the password reset emailing options.

- Adds 5 view decorators: @login_or_direct1_required, @login_or_direct2_required, @direct1_required, @direct2_required, @login_required

## Installation
Common_login is meant to be dropped into any Django project easily as a Django app:

1) Install the common_login app.

The best approach is to clone this repository into your existing GIT Repo (Django Project) as a submodule.
**NOTE:** Use SSH for this, as the remote repo path is checked into your main project, and for deployment, you want SSH.

```
git submodule add git@bitbucket.org:sparklab-ondemand/django_common_login.gitt [PATH_TO_APPS_DIRECTORY]/common_login
```
Then add the app to the INSTALLED_APPS in Django settings.

2) Map your URL as desired.
```python
urlpatterns = patterns('',
    ...
    url(r'^account/', include('path.to.common_login.urls')),
    ...
)
```

3) If supporting django users, within django admin (databases), set the correct Site domain and display name.

4) If supporting django users and ENABLE_PASSWORD_RESET_EMAIL=True, then emailing needs to be setup in your Django settings (EMAIL_HOST, EMAIL_PORT, etc...)

## Configuration
The few configuration settings are in the login_settings.py script.

#### ENABLE_PASSWORD_RESET_EMAIL
This setting can be disabled if you don't want to enable emailing from the django application.  Disabling this will remove the "Forgot Password?" link from the login page, and disable the associated views/urls.

#### PASSWORD_RESET_FROM_EMAIL
The 'FROM' email in the password reset flow.

####DIRECT2_AUTH_ENDPOINT
The endpoint used for Direct 2.x authentication.

#####DIRECT1_AUTH_TEST
The endpoint used for Direct 1.x api key validation.

## Django Sessions
If using any of the following decorators: @login_or_direct1_required, @login_or_direct2_required, @direct1_required, @direct2_required, then Django sessions need to be enabled.

### Direct 1.x
The following session vars will be set by the common_login app after verifying the API keys:

- 'direct1_api_key'
- 'direct1_username'
- 'direct1_password'

### Direct 2.x
The following session vars will be set by the common_login app after authenticating with Direct 2.0

- 'direct2_x_dnb_user'
- 'direct2_x_dnb_pwd'
- 'direct2_token'
- 'direct2_token_time'