__author__ = 'Bammel'
from django.contrib.auth.models import User

# Makes the base User email field required, unique, and not-null
User._meta.get_field('email').blank = False
User._meta.get_field('email').null = False
User._meta.get_field('email')._unique = True