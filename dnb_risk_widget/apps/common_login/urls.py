__author__ = 'Bammel'

from django.conf.urls import patterns, url
from django.contrib.auth.views import password_reset_confirm, password_reset_done, password_reset_complete, logout
from views import password_reset, login

import login_settings

urlpatterns = patterns('',
    url(r'^login/?$', login,
        {'template_name': 'common_login/login.html',
         'extra_context':{'ENABLE_PASSWORD_RESET_EMAIL': login_settings.ENABLE_PASSWORD_RESET_EMAIL}
        }, name='login'),
    url(r'^logout/?$', logout,
        { 'template_name': 'common_login/logout.html' }, name='logout')
)

if login_settings.ENABLE_PASSWORD_RESET_EMAIL:
    urlpatterns += patterns('',
        url(r'^reset_password/?$', password_reset, name='password_reset'),
        url(r'^reset_password_sent/?$', password_reset_done,
            {'template_name': 'common_login/reset_password_sent.html'}, name='password_reset_done'),
        url(r'^reset_password_confirm/(?P<uidb64>.+)/(?P<token>.+)$', password_reset_confirm,
            {'template_name': 'common_login/reset_password_confirm.html'}, name='password_reset_confirm'),
        url(r'^reset_password_complete', password_reset_complete,
            {'template_name': 'common_login/reset_password_complete.html'}, name='password_reset_complete')
    )