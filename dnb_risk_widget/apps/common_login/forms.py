__author__ = 'Bammel'

from django import forms
import direct_auth
import datetime

class Direct1Form(forms.Form):
    api_key = forms.CharField(max_length=100)
    username = forms.CharField(max_length=100)
    password = forms.CharField(max_length=100)

    def __init__(self, request=None, *args, **kwargs):
        """
        The 'request' parameter is set for custom auth use by subclasses.
        The form data comes in via the standard 'data' kwarg.
        """
        self.request = request
        super(Direct1Form, self).__init__(*args, **kwargs)

    def clean(self):
        """
        Will validate the user credentials entered by the user.
        """
        data = self.cleaned_data
        success = False
        try: # Attempt to login
            success = direct_auth.validate_direct1_credentials(data['api_key'], data['username'], data['password'])
        except:
            pass

        if not success:
            self.api_login_errors = True
            raise forms.ValidationError('Your API credentials were invalid, please try again.')

        # Set session vars
        self.request.session['direct1_api_key'] = data['api_key']
        self.request.session['direct1_username'] = data['username']
        self.request.session['direct1_password'] = data['password']

        return data

class Direct2Form(forms.Form):
    x_dnb_user = forms.CharField(max_length=100)
    x_dnb_pwd = forms.CharField(max_length=100)

    def __init__(self, request=None, *args, **kwargs):
        """
        The 'request' parameter is set for custom auth use by subclasses.
        The form data comes in via the standard 'data' kwarg.
        """
        self.request = request
        super(Direct2Form, self).__init__(*args, **kwargs)

    def clean(self):
        """
        Will validate the user credentials entered by the user, and grab an authentication token.
        """
        data = self.cleaned_data
        token = None
        try: # Attempt to login
            token = direct_auth.direct2_auth(data['x_dnb_user'], data['x_dnb_pwd'])
        except:
            pass

        if not token:
            self.api_login_errors = True
            raise forms.ValidationError('Your API credentials were invalid, please try again.')

        # Set session vars
        now_string = datetime.datetime.now().isoformat()
        try:
            # Strip microseconds from timestamp
            now_string = now_string[:now_string.index('.')]
        except:
            pass

        self.request.session['direct2_x_dnb_user'] = data['x_dnb_user']
        self.request.session['direct2_x_dnb_pwd'] = data['x_dnb_pwd']
        self.request.session['direct2_token'] = token
        self.request.session['direct2_token_time'] = now_string

        return data