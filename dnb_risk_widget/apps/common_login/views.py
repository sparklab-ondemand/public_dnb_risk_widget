__author__ = 'Bammel'

from django.contrib.auth import REDIRECT_FIELD_NAME, login as auth_login
from django.contrib.auth.forms import AuthenticationForm
from django.conf import settings
from django.template.response import TemplateResponse
from django.utils.http import is_safe_url
from django.shortcuts import resolve_url
from django.views.decorators.debug import sensitive_post_parameters
from django.views.decorators.cache import never_cache
from django.shortcuts import render
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.views.decorators.csrf import csrf_protect
from django.contrib.auth.forms import PasswordResetForm
from django.contrib.auth.tokens import default_token_generator
from django.contrib.auth.models import User
from django.template import loader
from django.utils.http import urlsafe_base64_encode
from django.contrib.sites.models import get_current_site
from django.utils.encoding import force_bytes
from django.core.mail import send_mail
import login_settings
import forms

@sensitive_post_parameters()
@csrf_protect
@never_cache
def login(request, template_name='registration/login.html',
          redirect_field_name=REDIRECT_FIELD_NAME,
          authentication_form=AuthenticationForm,
          current_app=None, extra_context=None):
    """
    Displays the login form and handles the login action.
    """
    redirect_to = request.REQUEST.get(redirect_field_name, '')

    if request.method == "POST":
        user_login = True # User login or API login
        # Pull the appropriate form
        if request.body.find('&direct1') != -1:
            authentication_form = forms.Direct1Form
            user_login = False
        elif request.body.find('&direct2') != -1:
            authentication_form = forms.Direct2Form
            user_login = False

        form = authentication_form(request, data=request.POST)
        if form.is_valid():

            # Ensure the user-originating redirection url is safe.
            if not is_safe_url(url=redirect_to, host=request.get_host()):
                redirect_to = resolve_url(settings.LOGIN_REDIRECT_URL)

            if user_login: # Only login to django on user login (not API Key Login).
                # Okay, security check complete. Log the user in.
                auth_login(request, form.get_user())

            return HttpResponseRedirect(redirect_to)
    else:
        form = authentication_form(request)

    current_site = get_current_site(request)

    context = {
        'form': form,
        redirect_field_name: redirect_to,
        'site': current_site,
        'site_name': current_site.name,
    }

    if 'api' in request.GET:
        context['api'] = request.GET['api']

    context['show_user_form'] = True
    if 'api_only' in request.GET:
        context['show_user_form'] = False


    if extra_context is not None:
        context.update(extra_context)
    return TemplateResponse(request, template_name, context,
                            current_app=current_app)


# Mostly taken from the standard django password reset machinery.
# This view was customized to assert that the email is tied to an existing user.
@csrf_protect
def password_reset(request):
    post_reset_redirect = reverse('password_reset_done')

    if request.method == "POST":
        form = PasswordResetForm(request.POST)
        if form.is_valid():
            try:
                user = User.objects.get(email = request.POST['email'])

                use_https = request.is_secure()
                current_site = get_current_site(request)

                site_name = current_site.name
                domain = current_site.domain

                c = {
                    'email': user.email,
                    'domain': domain,
                    'site_name': site_name,
                    'uid': urlsafe_base64_encode(force_bytes(user.pk)),
                    'user': user,
                    'token': default_token_generator.make_token(user),
                    'protocol': 'https' if use_https else 'http',
                }
                subject = loader.render_to_string('common_login/email/email_subject_template.txt', c)

                # Email subject *must not* contain newlines
                subject = ''.join(subject.splitlines())
                email = loader.render_to_string('common_login/email/email_template.txt', c)
                send_mail(subject, email, login_settings.PASSWORD_RESET_FROM_EMAIL, [user.email])

                return HttpResponseRedirect(post_reset_redirect)
            except Exception as e:
                return render(request, 'common_login/reset_password.html', {'form':form, 'form_errors':True})
    else:
        form = PasswordResetForm()
    context = {
        'form': form,
    }

    return render(request, 'common_login/reset_password.html', context)

