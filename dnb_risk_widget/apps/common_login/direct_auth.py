__author__ = 'Bammel'
import requests
import login_settings

def direct2_auth(uid, pwd):
    """
    Validates the Direct 2 credentials and returns an Auth Token.
    """
    headers = {"x-dnb-user":uid, "x-dnb-pwd":pwd}

    result = requests.post(login_settings.DIRECT2_AUTH_ENDPOINT, headers=headers)
    if result.status_code == 200:
        return result.headers["Authorization"]

    # Returned if authentication fails
    return None

def validate_direct1_credentials(api_key, uid, pwd):
    """
    Validates the Direct 1 credentials and returns True/False.
    """
    headers = {'API-KEY':api_key, 'username':uid, 'password':pwd}

    result = requests.get(login_settings.DIRECT1_AUTH_TEST, headers=headers)
    if result.status_code != 403:
        return True

    return False