__author__ = 'Bammel'

#admin.py
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User
from django.contrib import admin

admin.site.unregister(User)

# This code replaces the "add user" form in the admin panel, adding the email field, which is now required.
class CustomUserAdmin(UserAdmin):
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('username', 'email', 'password1', 'password2')}
        ),
    )

admin.site.register(User, CustomUserAdmin)
