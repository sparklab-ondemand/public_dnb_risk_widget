__author__ = 'Bammel'

# This setting can be disabled if you don't want to enable emailing from the django application.
# Disabling this will remove the "Forgot Password?" link from the login page, and disable the associated views/urls.
ENABLE_PASSWORD_RESET_EMAIL = True

# The sending email address of the password reset email.
PASSWORD_RESET_FROM_EMAIL = 'DNB SparkLab <sparklab@dnb.com>'

# The endpoint used for authorizing Direct 2 when API keys are supplied.
DIRECT2_AUTH_ENDPOINT = 'https://maxcvservices.dnb.com/rest/Authentication'

# The endpoint used for validating a Direct 1 API-Key, when supplied.
DIRECT1_AUTH_TEST = 'https://dnbdirect-api.dnb.com/DnBAPI-15/rest/'
