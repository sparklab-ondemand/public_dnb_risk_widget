__author__ = 'Bammel'

from functools import wraps
from django.conf import settings
from django.contrib.auth import REDIRECT_FIELD_NAME
from django.utils.decorators import available_attrs
from django.utils.encoding import force_str
from django.utils.six.moves.urllib.parse import urlparse
from django.shortcuts import resolve_url

def login_required(function=None, redirect_field_name=REDIRECT_FIELD_NAME, login_url=None, api=None, user_login=True):
    """
    Decorator for views that checks that the user is logged in, redirecting
    to the log-in page if necessary.
    """
    if not login_url:
        login_url = settings.LOGIN_URL

    user_test = lambda r: r.user.is_authenticated()

    # disable logging in with user, api-key only.
    if not user_login:
        user_test = lambda r: False
        login_url = _add_url_parameter(login_url, 'api_only', 'true')
        if not api: # must have an api - assign a default if none given
            api = 'direct2'

    if api == 'direct1':
        test_func = lambda r: user_test(r) or _test_direct1(r)
        login_url = _add_url_parameter(login_url, 'api', 'direct1')
    elif api == 'direct2':
        test_func = lambda r: user_test(r) or _test_direct2(r)
        login_url = _add_url_parameter(login_url, 'api', 'direct2')
    else:
        test_func = lambda r: r.user.is_authenticated()

    actual_decorator = _user_passes_test(
        lambda r: test_func(r),
        login_url=login_url,
        redirect_field_name=redirect_field_name
    )
    if function:
        return actual_decorator(function)
    return actual_decorator

def login_or_direct1_required(function=None, redirect_field_name=REDIRECT_FIELD_NAME, login_url=None):
    """
    Decorator for views that checks that the user is logged in or has provided direct1 credentials,
    redirecting to the log-in page if necessary.
    """
    return login_required(function, redirect_field_name, login_url, api='direct1')

def login_or_direct2_required(function=None, redirect_field_name=REDIRECT_FIELD_NAME, login_url=None):
    """
    Decorator for views that checks that the user is logged in or has provided direct2 credentials,
    redirecting to the log-in page if necessary.
    """
    return login_required(function, redirect_field_name, login_url, api='direct2')

def direct1_required(function=None, redirect_field_name=REDIRECT_FIELD_NAME, login_url=None):
    """
    Decorator for views that checks that the user has provided direct 1 credentials,
    redirecting to the log-in page if necessary.
    If username login is also required, see 'login_or_direct1_required' decorator.
    """
    return login_required(function, redirect_field_name, login_url, api='direct1', user_login=False)

def direct2_required(function=None, redirect_field_name=REDIRECT_FIELD_NAME, login_url=None):
    """
    Decorator for views that checks that the user has provided direct 2 credentials,
    redirecting to the log-in page if necessary.
    If username login is also required, see 'login_or_direct2_required' decorator.
    """
    return login_required(function, redirect_field_name, login_url, api='direct2', user_login=False)

def _user_passes_test(test_func, login_url=None, redirect_field_name=REDIRECT_FIELD_NAME, api=None):
    # internal function - modified from the original django source in auth/decorators.py

    def decorator(view_func):
        @wraps(view_func, assigned=available_attrs(view_func))
        def _wrapped_view(request, *args, **kwargs):
            if test_func(request):  # modified for getting session vars from request, modified from request.user
                return view_func(request, *args, **kwargs)
            path = request.build_absolute_uri()
            # urlparse chokes on lazy objects in Python 3, force to str
            resolved_login_url = force_str(
                resolve_url(login_url or settings.LOGIN_URL))
            # If the login url is the same scheme and net location then just
            # use the path as the "next" url.
            login_scheme, login_netloc = urlparse(resolved_login_url)[:2]
            current_scheme, current_netloc = urlparse(path)[:2]
            if ((not login_scheme or login_scheme == current_scheme) and
                (not login_netloc or login_netloc == current_netloc)):
                path = request.get_full_path()
            from django.contrib.auth.views import redirect_to_login
            return redirect_to_login(
                path, resolved_login_url, redirect_field_name)
        return _wrapped_view
    return decorator

def _test_direct1(request):
    # internal function - tests if session vars are in place
    session_vars = request.session.keys()
    if ('direct1_api_key' in session_vars) and ('direct1_username' in session_vars) and ('direct1_password' in session_vars):
        return True

    return False

def _test_direct2(request):
    # internal function - tests if session vars are in place
    session_vars = request.session.keys()
    if ('direct2_x_dnb_user' in session_vars) and ('direct2_x_dnb_pwd' in session_vars):
        return True

    return False


def _add_url_parameter(url, param_name, param_val):
    # adds a new get parameter to the end of the URL.  Looks for existing parameters and adds ? or & accordingly.
    separator = '&'

    q_index = url.find('?')
    if q_index == -1:
        separator = '?'

    return '%s%s%s=%s' % (url, separator, param_name, param_val)