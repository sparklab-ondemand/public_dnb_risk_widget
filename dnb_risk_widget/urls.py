from django.conf.urls import patterns, include, url
from django.contrib import admin
from dnb_risk_widget.apps.direct2_proxy import views as proxy_views

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

admin.autodiscover()

urlpatterns = patterns('',
    url(r'^account/', include('dnb_risk_widget.apps.common_login.urls')),
    url(r'^proxy/(.*)$', proxy_views.call_direct, name='direct2_proxy'),
    url(r'^admin/?', include(admin.site.urls)),
    url(r'/?(^.*)$', 'dnb_risk_widget.apps.credit_widget.views.app_view'),
)